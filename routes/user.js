var connection = require('../config/dbconnection').connection,
    md5 = require('md5'),
    responses = require('./responses'),
    commonFunc = require('./commonfunction'),
    config = require('../config/config'),
    constants = require('./constants'),
    needle = require('needle');

/*
 * ----------------------------------
 * CREATION OF USERS ACCOUNT
 * ----------------------------------
 */
exports.register_user = function (req, res) {
    var name = req.body.name,
        email = req.body.email,
        password = req.body.password;
    var manvalues = [name, email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserEmail(email, function (result) {
            if (result != 0) {
                responses.authenticationAlreadyExists(res);
                return;
            } else {
                if (password.length < config.AppPasswordLength) {
                    var response = {
                        "message": constants.responseMessages.APP_PASSWORD_ERROR,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    var encrypted_pass = md5(password),
                        access_token = md5(encrypted_pass + new Date());
                    var sql = "INSERT INTO `tb_users` (`name`,`password`,`email`,`access_token`,`last_login`,`creation_time`,`creation_date`)";
                    sql += " VALUES (?,?,?,?,?,?,?)";
                    connection.query(sql, [name, encrypted_pass, email, access_token, new Date(), new Date(), new Date()], function (err, result_insert) {
                        if (err) {
                            res.json({
                                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                "data": err
                            });
                        } else {
                            res.json({
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            });
                        }
                    });
                }
            }
        });

    }
}
/*
 * ----------------------------------
 * USER LOGIN
 * ----------------------------------
 */
exports.user_login = function (req, res) {
    var email = req.body.email,
        password = req.body.password;
    var manvalues = [email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    console.log("REQUEST === ",req.body);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT * FROM `tb_users` WHERE `email`=? LIMIT 1";
        connection.query(sql, [email], function (err, result) {
            if (err) {
                res.json({
                    "message": constants.responseMessages.ERROR_IN_EXECUTION,
                    "status": constants.responseFlags.ERROR_IN_EXECUTION,
                    "data": err
                });
            } else {
                if (result.length == 0) {
                    res.json({
                        "message": constants.responseMessages.EMAIL_NOT_EXISTS,
                        "status": constants.responseFlags.INVALID_EMAIL_ID,
                        "data": {}
                    });
                } else {
                    if (result[0].password != encrypted_pass) {
                        res.json({
                            "message": constants.responseMessages.WRONG_PASSWORD,
                            "status": constants.responseFlags.WRONG_PASSWORD,
                            "data": {}
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                }
            }
        });
    }
}

/*
 * ----------------------------------
 * INSERTION OF PICKUP ADDRESS
 * ----------------------------------
 */

exports.pickup_addr = function (req, res) {
    var accesstoken = req.body.accesstoken,
        address = req.body.address,
        isenabled = 1,
        pickuplat = req.body.pickuplat,
        pickuplong = req.body.pickuplong;

    var manvalues = [accesstoken, address];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                commonFunc.authenticateUserPickupAddress(user_id, address, function (result) {
                    if (result != 0) {
                        responses.AddressAlreadyExists(res);
                    } else {
                        var sql = "INSERT INTO `tb_pickup_address` (`user_id`,`address`,`isenabled`,`pickup_lat`,`pickup_long`,`creation_date`,`creation_time`)";
                        sql += " VALUES (?,?,?,?,?,?,?)";
                        connection.query(sql, [user_id, address, isenabled, pickuplat, pickuplong, new Date(), new Date()], function (err, result_insert) {
                            if (err) {
                                res.json({
                                    "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                    "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                    "data": err
                                });
                            } else {
                                res.json({
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": {}
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}

/*
 * ----------------------------------
 * GET ALL PICKUP ADDRESSES
 * ----------------------------------
 */

exports.get_pickup_addr = function (req, res) {
    var accesstoken = req.body.accesstoken;
    var manvalues = [accesstoken];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                var sql = "SELECT * FROM `tb_pickup_address` WHERE `user_id`= ? ";
                connection.query(sql, [user_id], function (err, result) {
                    if (err) {
                        res.json({
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": err
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                });

            }
        });
    }
}

/*
 * ----------------------------------
 * DELETE PICKUP ADDRESSES
 * ----------------------------------
 */

exports.del_pickup_addr = function (req, res) {
    var accesstoken = req.body.accesstoken,
        address_id = req.body.address_id
    var manvalues = [accesstoken,address_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                console.log("Pickup Address Delete Request For User == ",user_id);
                var sql = "DELETE FROM `tb_pickup_address` WHERE `user_id`= ? and id = ? LIMIT 1";
                connection.query(sql, [user_id, address_id], function (err, result) {
                    if (err) {
                        res.json({
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": err
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                });

            }
        });
    }
}

/*
 * ----------------------------------
 * INSERTION OF DELIVERY ADDRESS
 * ----------------------------------
 */

exports.delivery_addr = function (req, res) {
    var accesstoken = req.body.accesstoken,
        address = req.body.address,
        isenabled = 1,
        deliverylat = req.body.deliverylat,
        deliverylong = req.body.deliverylong;
    var manvalues = [accesstoken, address];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                commonFunc.authenticateUserDeliveryAddress(user_id, address, function (result) {
                    if (result != 0) {
                        responses.AddressAlreadyExists(res);
                        return;
                    } else {
                        var sql = "INSERT INTO `tb_delivery_address` (`user_id`,`address`,`isenabled`,`delivery_lat`,`delivery_long`,`creation_time`,`creation_date`)";
                        sql += " VALUES (?,?,?,?,?,?,?)";
                        connection.query(sql, [user_id, address, isenabled, deliverylat, deliverylong, new Date(), new Date()], function (err, result_insert) {
                            if (err) {
                                res.json({
                                    "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                    "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                    "data": err
                                });
                            } else {
                                res.json({
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": {}
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}

/*
 * ----------------------------------
 * GET ALL DELIVERY ADDRESSES
 * ----------------------------------
 */

exports.get_delivery_addr = function (req, res) {
    var accesstoken = req.body.accesstoken;
    var manvalues = [accesstoken];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                var sql = "SELECT * FROM `tb_delivery_address` WHERE `user_id`= ? ";
                connection.query(sql, [user_id], function (err, result) {
                    if (err) {
                        res.json({
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": err
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                });

            }
        });
    }
}

/*
 * ----------------------------------
 * DELETE Delivery ADDRESSES
 * ----------------------------------
 */

exports.del_deliv_addr = function (req, res) {
    var accesstoken = req.body.accesstoken,
        address_id = req.body.address_id
    var manvalues = [accesstoken,address_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                console.log("Delivery Address Delete Request For User == ",user_id);
                var sql = "DELETE FROM `tb_delivery_address` WHERE `user_id`= ? and `id`=? LIMIT 1";
                connection.query(sql, [user_id, address_id], function (err, result) {
                    if (err) {
                        res.json({
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": err
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                });

            }
        });
    }
}
/*
 * ----------------------------------
 * UPDATE STATIC EMAIL AND PHONE
 * ----------------------------------
 */

exports.update_defaults = function (req, res) {
    var accesstoken = req.body.accesstoken,
        email = req.body.email,
        phone = req.body.phone
    var manvalues = [accesstoken,email,phone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                console.log("UPDATE DEFAULT Request For User == ",user_id);
                var sql = "UPDATE `tb_users` SET `static_email`=?,`static_phone`=? WHERE `user_id`= ? LIMIT 1";
                connection.query(sql, [email, phone, user_id], function (err, result) {
                    if (err) {
                        res.json({
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": err
                        });
                    } else {
                        res.json({
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result
                        });
                    }
                });

            }
        });
    }
}

/*
 * ----------------------------------
 * INSERTION OF ORDERS
 * ----------------------------------
 */

exports.orders = function (req, res) {
    var orderid = req.body.orderid,
        accesstoken = req.body.accesstoken,
        pickupid = parseInt(req.body.pickupid),
        deliveryid = parseInt(req.body.deliveryid),
        orderdetails = req.body.orderdetails,
        specialinstruction = req.body.specialinstruction,
        recepientname = req.body.recepientname,
        deliverytime = req.body.deliverytime,
        timezone = req.body.timezone,
        pickupaddr = req.body.pickupaddr,
        pickupname = req.body.pickupname,
        pickuptime = req.body.pickuptime,
        pickuplat = req.body.pickuplat,
        pickuplong = req.body.pickuplong,
        deliveryaddress = req.body.deliveryaddress,
        deliverylat = req.body.deliverylat,
        deliverylong = req.body.deliverylong,
        email = req.body.email,
        phone = req.body.phone,
        company_name = req.body.company_name,
        job_desc = "Order Id : " + (typeof orderid === 'undefined' ? "-" : orderid) + "\n" +
                    "Order Details : " + (typeof orderdetails === 'undefined' ? "-" : orderdetails) + "\n" +
                    "Company Name : " + (typeof company_name === 'undefined' ? "-" : company_name) + "\n" +
                    "Receipent`s Name : " + (typeof recepientname === 'undefined' ? '-' : recepientname) + "\n" +
                    "Pickup Between Time : " + (typeof req.body.pickupduration === 'undefined' ? "-" : req.body.pickupduration) + "\n" +
                    "Delivery Between Time : " + (typeof req.body.deliveryduration === 'undefined' ? "-" : req.body.deliveryduration) + "\n" +
                    "Special Instructions : " +  (typeof specialinstruction === 'undefined' ? "-" : specialinstruction) ;
    console.log("Order Request == ",req.body);
    var manvalues = [accesstoken, pickupid, deliveryid, orderdetails, recepientname, deliverytime, deliveryaddress, pickupaddr];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccesstoken(accesstoken, function (authenticateUserAccessTokenResult) {
            if (authenticateUserAccessTokenResult == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = authenticateUserAccessTokenResult[0].user_id;
                if (deliveryid == 0){
                    var sql = "INSERT INTO `tb_delivery_address` (`user_id`,`address`,`isenabled`,`delivery_lat`,`delivery_long`,`creation_time`,`creation_date`)";
                    sql += " VALUES (?,?,?,?,?,?,?)";
                    connection.query(sql, [user_id, deliveryaddress, 1, deliverylat, deliverylong, new Date(), new Date()], function (err, result_insert) {
                        if (err) {
                            res.json({
                                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                "data": err
                            });
                        } else {
                            deliveryid = result_insert.insertId;
                            create_task()
                        }
                    });
                }else{
                    create_task()
                }
                function create_task() {
                    var sql = "INSERT INTO `tb_orders` (`order_id`,`user_id`,`pickup_id`,`pickup_address`,`pickup_name`,`pickup_time`,`pickup_lat`,`pickup_long`,`delivery_id`,`order_details`,`special_instruction`,`recepient_name`,`delivery_address`,`delivery_lat`,`delivery_long`,`delivery_time`,`creation_date`,`creation_time`)";
                    sql += " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    connection.query(sql, [orderid, user_id, pickupid, pickupaddr, pickupname, pickuptime, pickuplat, pickuplong, deliveryid, orderdetails, specialinstruction, recepientname, deliveryaddress, deliverylat, deliverylong, deliverytime, new Date(), new Date()], function (err, result_insert) {
                        console.log("result_insert == ", result_insert, err);
                        if (err) {
                            res.json({
                                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                "data": err
                            });
                        } else {
                            res.json({
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            });
                            //-----------------POST Request to TOOKAN------------------------

                            var postbody = {
                                "access_token": "a4fbdb0835a288fe15047dd9b3c4fdb8",
                                "job_description": job_desc,
                                "job_pickup_phone": phone,
                                "job_pickup_name": pickupname,
                                "job_pickup_email": email,
                                "job_pickup_address": pickupaddr,
                                "job_pickup_latitude": pickuplat,
                                "job_pickup_longitude": pickuplong,
                                "job_pickup_datetime": pickuptime,
                                "customer_email": email,
                                "customer_username": recepientname,
                                "customer_phone": phone,
                                "customer_address": deliveryaddress,
                                "latitude": deliverylat,
                                "longitude": deliverylong,
                                "job_delivery_datetime": deliverytime,
                                "has_pickup": "1",
                                "has_delivery": "1",
                                "layout_type": "0",
                                "tracking_link": 0,
                                "timezone": timezone
                            }
                            needle.post('https://api.tookanapp.com:8888/create_task', postbody,
                                function (err, resp, body) {
                                    console.log("------------")
                                    console.log(body);
                                });
                            //------------------Finish Request POST to TOOKAN-----------
                        }
                    });
                }
            }
        });
    }
}